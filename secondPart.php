<?php

$databasehost = "localhost";
$databasename = "gactech";
$databasetable = "appels";
$databaseusername="root";
$databasepassword = "";

try {
    $pdo = new PDO("mysql:host=$databasehost;dbname=$databasename",
        $databaseusername, $databasepassword,
        array(
            PDO::MYSQL_ATTR_LOCAL_INFILE => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    );
} catch (PDOException $e) {
    die("database connection failed: ".$e->getMessage());
}


/**
 * la durée totale réelle des appels effectués après le 15/02/2012 (inclus)
 */
$sql= "select sum(volume_reel) as total from appels where type like '%appel%' and date_appel >= '2012-02-15'";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$totalCalls = (int) $stmt->fetchColumn();
echo "la durée totale réelle des appels effectués après le 15/02/2012 est : " . $totalCalls." appel\n\n";


/**
 * le TOP 10 des volumes data facturés en dehors de la tranche horaire 8h00-18h00, par abonné
 */
$sql = "SELECT numero_abonne, sum(volume_facture) as totalData FROM `appels` where type like '%connexion%' and (heure_appel < '18:00:00' and heure_appel > '08:00:00') group by numero_abonne order by totalData desc LIMIT 10";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$topDataFacture = $stmt->fetchAll(PDO::FETCH_OBJ);
echo "le TOP 10 des volumes data facturés en dehors de la tranche horaire 8h00-18h00, par abonné sont : \n\n";
foreach($topDataFacture as $dataFacture) {
    echo "Numéro abonné : " .$dataFacture->numero_abonne.", Total facturé : ".$dataFacture->totalData."\n";
}


/**
 * la quantité totale de SMS envoyés par l'ensemble des abonnés
 */
$sql= "SELECT count(*) as total FROM `appels` WHERE type like '%SMS%'";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$totalSMS = (int) $stmt->fetchColumn();
echo "\nla quantité totale de SMS envoyés par l'ensemble des abonnés est : " . $totalSMS." SMS\n\n";
?>
