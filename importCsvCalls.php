<?php

$csvfile = $argv[1];

if(!file_exists($csvfile)) {
    die("File not found. Make sure you specified the correct path.");
}

$file_parts = pathinfo($csvfile);
if($file_parts['extension'] != 'csv'){
    die("File extension is not correct, please choose an csv file.");
}
$time = microtime(true);
$mem = memory_get_usage();
import($csvfile);
$execution = microtime(true) - $time;
echo "\n\n";
echo 'Execution time : '.  $execution .' seconds';
echo "\n";
echo 'memory usage : ' . (memory_get_usage() - $mem) / (1024 * 1024) .' M';

function import($filePath){
    if (($handle = fopen($filePath, "r")) !== FALSE)
    {
        $values = array();
        $row =1;
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
        {
            if($row < 4){
                $row++;
                continue;
            }
            $numCompte = $data[0];
            $numFacture = $data[1];
            $numAbonne = $data[2];
            $dateAppel = DateTime::createFromFormat('d/m/Y', $data[3]);
            $heureAppel = $data[4];
            $volumeReel = $data[5];
            $volumeFacture = $data[6];
            $type = $data[7];
            $values[] = "(".$numCompte.",".$numFacture.",".$numAbonne.",'".$dateAppel->format('Y-m-d')."','".$heureAppel."','".$volumeReel."','".$volumeFacture."','".addslashes($type)."')";
        }

        if(!empty($values)){
            $con=mysqli_connect("localhost","root","","gactech");
            if (mysqli_connect_errno())
            {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }
            $result =  mysqli_query($con,"INSERT INTO appels  VALUES " .implode(',',$values));
            if ($result) {
                echo "File Imported successfully";
            } else {
                echo ("Could not insert data : " . mysqli_error($con));
            }
        }
        else{
            echo 'File is empty';
        }
        fclose($handle);
    }

}