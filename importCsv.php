<?php

if(count($argv) == 1) {
    die("No file passed in parameters, please specify the csv file path");
}

// First part, Extract Data, Load into Mysql
$databasehost = "localhost";
$databasename = "gactech";
$databasetable = "appels";
$databaseusername="root";
$databasepassword = "";
$fieldseparator = ";";
$lineseparator = "\n";
$csvfile = $argv[1];

if(!file_exists($csvfile)) {
    die("File not found. Make sure you specified the correct path.");
}
$time_start = microtime(true);
try {
    $pdo = new PDO("mysql:host=$databasehost;dbname=$databasename",
        $databaseusername, $databasepassword,
        array(
            PDO::MYSQL_ATTR_LOCAL_INFILE => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    );
} catch (PDOException $e) {
    die("database connection failed: ".$e->getMessage());
}

/**
 *  Using LOAD LOCAL INFILE METHOD
 */
$affectedRows = $pdo->exec("
    LOAD DATA LOCAL INFILE ".$pdo->quote($csvfile)." INTO TABLE `$databasetable` 
      FIELDS TERMINATED BY ".$pdo->quote($fieldseparator)."
      LINES TERMINATED BY ".$pdo->quote($lineseparator)." IGNORE 3 LINES 
      (numero_compte, numero_facture, numero_abonne, @date_appel, heure_appel, volume_reel,volume_facture, type) 
      set date_appel  = STR_TO_DATE(@date_appel, '%d/%m/%Y')");

$execution_time = (microtime(true) - $time_start);

echo "Loaded a total of $affectedRows records from this csv file. in $execution_time seconds\n";

?>
